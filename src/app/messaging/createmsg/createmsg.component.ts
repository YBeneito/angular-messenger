import { MessagesService } from './../service/messages.service';
import { Messages } from './../models/messages';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-createmsg',
  templateUrl: './createmsg.component.html',
  styleUrls: ['./createmsg.component.css']
})
export class CreatemsgComponent implements OnInit {

  message: Messages = { }


  constructor(private MessagesService: MessagesService) {

  }

  ngOnInit(): void {
  }

  // Envoies les infos passées au formulaire à messagesService
  send(): void{
    this.message.read = false;
    console.log(this.message);
    this.MessagesService.create(this.message)
  }

  // Vide tous les champs du formulaire
  clear(): void{
    this.message = {};
    console.log(this.message);
  }

}


