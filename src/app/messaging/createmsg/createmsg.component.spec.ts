import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatemsgComponent } from './createmsg.component';

describe('CreatemsgComponent', () => {
  let component: CreatemsgComponent;
  let fixture: ComponentFixture<CreatemsgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatemsgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatemsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
