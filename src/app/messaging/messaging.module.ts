import { NotificationComponent } from './notification/notification.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CreatemsgComponent } from './createmsg/createmsg.component';
import { InboxComponent } from './inbox/inbox.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    InboxComponent,
    CreatemsgComponent,
    NotificationComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
  ],
  exports: [
    InboxComponent,
    CreatemsgComponent,
    NotificationComponent,
  ]
})
export class MessagingModule { }
