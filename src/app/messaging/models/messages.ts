export interface Messages {
  id?: Number,
  from?: String,
  to?: String,
  subject?: String,
  body?: String,
  read?: Boolean,
  // sendDate: Date,
}
