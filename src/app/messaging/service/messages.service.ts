import { Messages } from './../models/messages';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class MessagesService {

  public messagesObserver: BehaviorSubject<Messages[]>

  constructor(private http: HttpClient, private router: Router) {

      this.messagesObserver = new BehaviorSubject([] as Messages[])
  }

  async load() {
    let messages = await this.http.get('https://messagesapinode.herokuapp.com/msg').toPromise()
  }

   getAll(): any{
      return this.http
      .get<Messages[]>("https://messagesapinode.herokuapp.com/msg")
      .toPromise()
      .then(messages => this.messagesObserver.next(messages));
    };


    // getById(id: number): Messages | undefined{
    //   return this.messages.find(m => m.id = id);
    // };

    create(message: Messages): void{
      this.http.post("https://messagesapinode.herokuapp.com/msg", message)
               .toPromise()
               .then((data: any)=> {
                  console.log(data);
                  console.log(JSON.stringify(data.json));
                  this.refresh()
      })
              .catch(reason => {
                console.log(reason)
              })
    };

    // update(id: number): void{
    //   this.messages.find(m => m.id = id);
    // };

    refresh() {
      this.http.get<Messages[]>('https://messagesapinode.herokuapp.com/msg').toPromise().then(messages => {
      this.messagesObserver.next(messages)
      return messages
      })
    }
}
