import { MessagesService } from './../service/messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  unreads!: Number

  constructor(private MessagesService: MessagesService) {   }

  ngOnInit(): void {
    this.MessagesService.messagesObserver.subscribe(messages => {
      messages = messages.filter(msg=>msg.read === false)
      this.unreads = messages.length
    })
   }

}
