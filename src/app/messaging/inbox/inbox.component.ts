import { MessagesService } from './../service/messages.service';
import { Messages } from './../models/messages';
import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  // Déclaration de messages en tant que liste de messages
  @Input() messages!: Messages[];

  // Count represente le nombre de message à afficher
  @Input() count: number = 0;


  // Nombre de messages non lus
  @Input() unreads: number = 0;

  constructor(
    private MessagesService: MessagesService) {
    }

    // A l'initialisation , récupère la liste des messages venant de l'api et s'inscrit à l'observer
  ngOnInit(): void {
      this.MessagesService.getAll()
      this.MessagesService.messagesObserver.subscribe( messages => {
      this.messages = messages
    })
  }

    // Permet de trier les messages en fonction de leur lecture
  filter(event: Event){
    console.log(event);
    // Cast de l'elem pour le passer en tant qu'obj
    let input = event.target as HTMLInputElement;
    // renvois une liste de message où la propriété read = false en fonction de l'input
    if (input.checked) {
      this.messages = this.messages.filter(message => message.read === false)
    } else {
      this.MessagesService.refresh();
    }

  }
  // Raffraichi la liste des messages
  refresh(event: Event) {
    this.MessagesService.refresh()
  }

  // Récupère le nombre de messages non lu pour les notif
  getUnreads(): number {
    let notif = this.messages.filter(message => message.read === false).length
    console.log(notif)
    return notif
  }
}
