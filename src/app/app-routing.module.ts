import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CreatemsgComponent } from './messaging/createmsg/createmsg.component';
import { InboxComponent } from './messaging/inbox/inbox.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';

const ROUTES = [
  {
    path: "", component: DashboardComponent
  },
  {
    path: "inbox", component: InboxComponent
  },
  {
    path: "new-message", component: CreatemsgComponent
  }
] as Route[];

@NgModule({
  imports:  [
    RouterModule.forRoot(ROUTES),
  ],

  exports: [
    RouterModule,


  ],
})
export class AppRoutingModule { }
